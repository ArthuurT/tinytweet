import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { element } from '@angular/core/src/render3';
import { SelectMultipleControlValueAccessor } from '@angular/forms';


interface Tweet{
  id : string;
  body : string;
  sender : string;
  date : string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title : string = 'Tinytweet';
  URL : string = 'https://tinytwittertp.appspot.com/_ah/api/tinytwitter/v1/';
  nbUsers : number;
  nbMsgWrite : number;
  msgBody : string;
  curId : string[] = [];
  tag : string;
  pseudoWT : string;
  pseudoGT : string;
  pseudoFollow : string;
  pseudoToFollow : string;
  pseudoList = [];
  timeline : Tweet[] = [];
  timeCreated : number = 0;
  timePrinted : number;
  timelinePrinted : number;
  timeWriten: number;
  timeFollow : number;
  timeTag : number = 0;
  timelinePrintedByTag: number;
  timelineByTag: Tweet[] = [];
  searchTag: string;
  randomNumber: number;
  randomWinNumber: number;
  nbMsgTimeline: number;
  nbMsgTimelinebyTag: number;

  constructor(private http : HttpClient){}

  getUsers(){
    let t0 = new Date().getTime();
    this.http.get(this.URL + 'users/?limit=100')
    .subscribe((response) => {
      let t1 = new Date().getTime();
      this.timePrinted = (t1 - t0);
      this.pseudoList = [];
      response['items'].forEach(element => {
        this.pseudoList.push(element['pseudo']);
      });
    });
  }

  addUser(pseudo : string){
    let t0 = new Date().getTime();
    this.http.put(this.URL + 'users/' + pseudo,{})
    .subscribe(
      (response) => {
        console.log(pseudo + " : " + response);
        let t1 = new Date().getTime();
        this.timeCreated += (t1 - t0);
      },
      (erreur) => {
        console.log(pseudo + " : " + erreur);
        let t1 = new Date().getTime();
        this.timeCreated += (t1 - t0);
      }
    )
  }

  addMultipleUsers(){
    this.timeCreated = 0;
    for (let i = 0; i < this.nbUsers; i++) {
      this.addUser('user' + i.toString());
    }
  }

  removeAll(){
    this.timeCreated = null;
    this.timeWriten = null;
    this.timelinePrinted = null;
    this.timeFollow = null;
    this.timePrinted = null;
    this.timeTag = null;
    this.timelinePrintedByTag = null;
    this.http.delete(this.URL + 'users/')
    .subscribe((response) => {
      console.log("Base effacée");
    });
  }

  writeTweet(){
    let t0 = new Date().getTime();
    this.http.post(this.URL + 'users/' + this.pseudoWT + '/timeline?message=' + this.msgBody,{})
    .subscribe((response) => {
      let t1 = new Date().getTime();
      this.timeWriten += (t1 - t0);
      this.curId.push(response['id']);
    });
  }

  writeMultipleTweet(){
    this.timeWriten = 0;
    for (let i = 0; i < this.nbMsgWrite; i++) {
      this.writeTweet(); 
   }
  }

  printTimeline(){
    let t0 = new Date().getTime();
    this.http.get(this.URL + 'users/' + this.pseudoGT + '/timeline?limit=' + this.nbMsgTimeline.toString())
    .subscribe((response) => {
      let t1 = new Date().getTime();
      this.timelinePrinted = (t1 - t0);
      this.timeline = [];
      response['items'].forEach(element => {
        this.timeline.push({
          'id' : element['id'],
          'body' : element['body'],
          'sender' : element['sender']['pseudo'],
          'date' : element['publicationDate']
        });
      });
    });
  }

  printTimelineByTag(){
    let t0 = new Date().getTime();
    this.http.get(this.URL + 'messages?tag=' + this.searchTag + "&limit=" + this.nbMsgTimelinebyTag.toString())
    .subscribe((response) => {
      let t1 = new Date().getTime();
      this.timelinePrintedByTag = (t1 - t0);
      this.timelineByTag = [];
      response['items'].forEach(element => {
        this.timelineByTag.push({
          'id' : element['id'],
          'body' : element['body'],
          'sender' : element['sender']['pseudo'],
          'date' : element['publicationDate']
        });
      });
    });
  }

  follow(){
    let t0 = new Date().getTime();
    this.http.put(this.URL + 'users/' + this.pseudoFollow + '/followees/' + this.pseudoToFollow,{})
    .subscribe((response) => {
      let t1 = new Date().getTime();
      this.timeFollow = (t1 - t0);
    });
  }

  addTag(){
    let t0 = new Date().getTime();
    console.log(this.curId);
    this.curId.forEach(element => {
      this.http.patch(this.URL + 'messages/' + element + '?tag=' + this.tag,{})
      .subscribe((response) => {
        let t1 = new Date().getTime();
        this.timeTag += (t1 - t0);
      });
    });
  }

  randomFollow(){
    for (let i = 0; i < this.randomNumber; i++) {
      this.http.put(this.URL + 'users/' + this.pseudoFollow + '/followees/' + 'user' + i.toString(),{})
      .subscribe((response) => {
        console.log(i.toString() + " : " + response);
      });
      //this.sleep(1000);
    }
  }

  winRandomFollowers(){
    for (let i = 0; i < this.randomWinNumber; i++) {
      this.http.put(this.URL + 'users/' + 'user' + i.toString() + '/followees/' + this.pseudoFollow,{})
      .subscribe((response) => {
        console.log(response);
      }); 
    }
  }

  sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }


}
