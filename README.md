# Tinytweet

Tinytwit est une application de microblogging inspirée de twitter. Il s'agit donc de permettre à un utilisateur d'envoyer des messages à un ensemble de "followers". Attention, vous pouvez avoir des millions de followers.

## Configuration et Utilisation en local

```
git clone git@bitbucket.org:ArthuurT/tinytweet.git
```
```
cd tinytweet
```
```
ng serve --open
```
## URL Google App Engine utilisé

https://tinytwittertp.appspot.com/_ah/api/tinytwitter/v1/

## Code source (partie serveur)

https://github.com/Lastshot97/WebAndCloud_tinytwitter

## Résultats & Performances

### ___Temps pour poster un tweet en fonction du nombre de followers (en ms)___

* 100 followers -> Moyenne : 1071.1 / Variance : 151 432

* 1000 followers -> Moyenne : 997.4 / Variance : 46 913

### ___Temps pour obtenir sa timeline en fonction du nombre de followees (en ms)____

* 100 followers -> 10 messages -> Moyenne : 386 / Variance : 52 444
* 100 followers -> 50 messages -> Moyenne : 527 / Variance : 44 995
* 100 followers -> 100 messages -> Moyenne : 604 / Variance : 39 983
* 1000 followers -> 10 messages -> Moyenne : 522 / Variance : 123 149
* 1000 followers -> 50 messages -> Moyenne : 582 / Variance : 65 397
* 1000 followers -> 100 messages -> Moyenne : 559 / Variance : 55 328

### ___Temps pour obtenir les 50 derniers messages concernant un hashtag contenant n messages____

* 1000 messages -> Moyenne : 520 / Variance : 74 054


## Contributeurs

* AIMONIER-DAVAT Julien
* TROTTIER Arthur
* COGAN Julien